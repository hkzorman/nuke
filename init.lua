 -- Nuke Mod 2.1 by sfan5
-- code licensed under MIT

local all_tnt = {}
local tube_entry_image = ""
if minetest.get_modpath("pipeworks") then
	tube_entry_image = "^pipeworks_tube_connection_stony.png"
end

-- Effects
local function add_effects(pos, radius)
	minetest.add_particle({
		pos = pos,
		velocity = vector.new(),
		acceleration = vector.new(),
		expirationtime = 0.4,
		size = radius * 10,
		collisiondetection = false,
		vertical = false,
		texture = "tnt_boom.png",
		glow = 15,
	})
	minetest.add_particlespawner({
		amount = 32, -- 64
		time = 0.5,
		minpos = vector.subtract(pos, radius / 2),
		maxpos = vector.add(pos, radius / 2),
		minvel = {x = -10, y = -10, z = -10},
		maxvel = {x = 10, y = 10, z = 10},
		minacc = vector.new(),
		maxacc = vector.new(),
		minexptime = 1,
		maxexptime = 2.5,
		minsize = radius * 3,
		maxsize = radius * 5,
		texture = "tnt_smoke.png",
	})
end

local function spawn_tnt(pos, entname)
	minetest.sound_play("nuke_ignite", {pos = pos, gain = 1.0, max_hear_distance = 8})
	if not pos then return end
	return minetest.add_entity(pos, entname)
end

local function calculate_velocity(distance, tntradius, mult)
	-- (d is the distance vector)
	--            tntradius - | d |
	-- vel = d * ------------------- * mult
	--              tntradius - 1
	return vector.multiply(vector.multiply(distance, (tntradius - vector.length(distance)) / (tntradius - 1)), mult)
end

local function activate_if_tnt(nodename, nodepos, tntpos, tntradius)
	local explodetime_short = 4 -- seconds
	local explodetime_vary = 1.5
	if table.indexof(all_tnt, nodename) == -1 then
		return
	end
	local obj = spawn_tnt(nodepos, nodename)
	if obj then
		obj:setvelocity(calculate_velocity(vector.subtract(nodepos, tntpos), tntradius, {x=3, y=5, z=3}))
		obj:get_luaentity().timer = explodetime_short + math.random(-explodetime_vary, explodetime_vary)
	end
end

local function apply_tnt_physics(tntpos, tntradius)
	local objs = minetest.get_objects_inside_radius(tntpos, tntradius)
	for _, obj in ipairs(objs) do
		if obj:is_player() then
			if obj:get_hp() > 0 then
				obj:set_hp(obj:get_hp() - 1)
			end
		else
			local mult = {x=1.5, y=2.5, z=1.5}
			if table.indexof(all_tnt, obj:get_entity_name()) ~= -1 then
				mult = vector.multiply(mult, 2) -- apply more knockback to tnt entities
			end
			local vel = vector.add(obj:getvelocity(), calculate_velocity(vector.subtract(obj:getpos(), tntpos), tntradius, mult))
			obj:setvelocity(vel)
		end
	end
end


local function register_tnt(nodename, desc, tex, on_explode, explosion_on_collision)
	local explodetime = 10 -- seconds
	local collision_check_time = 0.5 
	local texfinal
	if type(tex) == "table" then
		texfinal = tex
	else
		texfinal = {tex.."_top.png", tex.."_bottom.png", tex.."_side.png"}
	end
	minetest.register_node(nodename, {
		tiles = texfinal,
		diggable = false,
		description = desc,
		groups = {nuke_tnt = 1},
		mesecons = {
			effector = {
				action_on = function(pos, node)
					minetest.remove_node(pos)
					spawn_tnt(pos, node.name)
					--nodeupdate(pos)
				end,
				action_off = function(pos, node) end,
				action_change = function(pos, node) end,
			},
		},
		on_punch = function(pos, node, puncher)
			minetest.remove_node(pos)
			spawn_tnt(pos, node.name)
			--nodeupdate(pos)
		end,
	})
	local entity = {
		physical = true, -- collision
		collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
		visual = "cube",
		textures = {texfinal[1], texfinal[2], texfinal[3], texfinal[3], texfinal[3], texfinal[3]},
		health = 1, -- number of punches required to defuse

		timer = 0,
		explode_on_collision = explosion_on_collision,
		collision_check_timer = 0,
		blinktimer = 0,
		blinkstatus = true,
	}
	function entity:on_activate(staticdata)
		self.object:setvelocity({x=0, y=4, z=0})
		self.object:setacceleration({x=0, y=-10, z=0}) -- gravity
		self.object:settexturemod("^[brighten")
	end
	function entity:on_step(dtime)
		self.timer = self.timer + dtime
		
		if self.explode_on_collision and self.prev_vel and self.prev_pos then
			local current_vel = self.object:get_velocity()
			if current_vel.x ~= self.prev_vel.x or current_vel.z ~= self.prev_vel.z then
				local explode_pos = vector.add(vector.normalize(self.prev_vel), self.prev_pos)	
				on_explode(vector.round(explode_pos))
				self.object:remove()
			end
		end
		
		self.prev_vel = self.object:get_velocity()
		self.prev_pos = self.object:get_pos()
		
		local mult = 1
		if self.timer > explodetime * 0.8 then -- blink faster before explosion
			mult = 4
		elseif self.timer > explodetime * 0.5 then
			mult = 2
		end
		self.blinktimer = self.blinktimer + mult * dtime

		if self.blinktimer > 0.5 then -- actual blinking
			self.blinktimer = self.blinktimer - 0.5
			if self.blinkstatus then
				self.object:settexturemod("")
			else
				self.object:settexturemod("^[brighten")
			end
			self.blinkstatus = not self.blinkstatus
		end

		if self.timer > explodetime then -- boom!
			on_explode(vector.round(self.object:getpos()))
			self.object:remove()
		end
		
	end
	function entity:on_punch(hitter)
		self.health = self.health - 1
		if self.health == 0 then -- give tnt node back if defused
			self.object:remove()
			if not minetest.setting_getbool("creative_mode") then
				hitter:get_inventory():add_item("main", nodename)
			end
		end
	end
	minetest.register_entity(nodename, entity)
	all_tnt[#all_tnt + 1] = nodename
end


local function on_explode_normal(pos, range)
	minetest.sound_play("nuke_explode", {pos = pos, gain = 1.0, max_hear_distance = 32})
	local nd = minetest.registered_nodes[minetest.get_node(pos).name]
	if nd ~= nil and nd.groups.water ~= nil then
		apply_tnt_physics(pos, range)
		return -- cancel explosion
	end

	add_effects(pos, range)

	local min = {x=pos.x-range,y=pos.y-range,z=pos.z-range}
	local max = {x=pos.x+range,y=pos.y+range,z=pos.z+range}
	local vm = minetest.get_voxel_manip()
	local emin, emax = vm:read_from_map(min,max)
	local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}
	local data = vm:get_data()
	local air = minetest.get_content_id("air")

	local content_id = minetest.get_name_from_content_id

	for x=-range, range do
	for y=-range, range do
	for z=-range, range do
		if vector.distance(pos, vector.add(pos, {x=x, y=y, z=z})) <= range then
			--local nodepos = vector.add(pos, {x=x, y=y, z=z})

			local p_pos = area:index(pos.x+x,pos.y+y,pos.z+z)



			--local n = minetest.get_node(nodepos)
			local n = content_id(data[p_pos])

			--if n.name ~= "air" then
			if n ~= "air" then
				activate_if_tnt(n, nodepos, pos, range)
				--minetest.remove_node(nodepos)
				data[p_pos] = air
			end
		end
	end
	end
	end

	vm:set_data(data)
	vm:write_to_map()
	vm:update_map()

	apply_tnt_physics(pos, range)
end

local function on_explode_split(pos, range, entname)
	minetest.sound_play("nuke_explode", {pos = pos, gain = 1.0, max_hear_distance = 16})
	for x=-range, range do
	for z=-range, range do
		if x*x+z*z <= range * range then
			local nodepos = vector.add(pos, {x=x, y=0, z=z})
			minetest.add_entity(nodepos, entname)
		end
	end
	end
end


-- Normal TNT
register_tnt("nuke:tnt", "Normal TNT", "tnt", function(pos)
	on_explode_normal(pos, 2)
end)

register_tnt("nuke:shell_tnt", "Exploding Shell", "nuke_tnt_shell", function(pos)
	on_explode_normal(pos, 2)
end, true)

minetest.register_craft({
	output = "nuke:tnt 4",
	recipe = {
		{"", "default:obsidian", ""},
		{"default:obsidian_block", "tnt:tnt_stick", "default:obsidian_block"},
		{"", "defaul:obsidian", ""},
	}
})

-- Iron TNT

register_tnt("nuke:iron_tnt", "Iron TNT", "nuke_iron_tnt", function(pos)
	on_explode_normal(pos, 6)
end)

minetest.register_craft({
	output = "nuke:iron_tnt 4",
	recipe = {
		{"", "group:wood", ""},
		{"default:steel_ingot", "default:coal_lump", "default:steel_ingot"},
		{"", "group:wood", ""},
	}
})


register_tnt(
	"nuke:iron_tntx", "Extreme Iron TNT",
	{"nuke_iron_tnt_top.png", "nuke_iron_tnt_bottom.png", "nuke_iron_tnt_side_x.png"},
	function(pos)
		on_explode_split(pos, 3, "nuke:iron_tnt")
	end
)

minetest.register_craft({
	output = "nuke:iron_tntx 1",
	recipe = {
		{"", "default:coal_lump", ""},
		{"default:coal_lump", "nuke:iron_tnt", "default:coal_lump"},
		{"", "default:coal_lump", ""},
	}
})

-- Mese TNT

register_tnt("nuke:mese_tnt", "Mese TNT", "nuke_mese_tnt", function(pos)
	on_explode_normal(pos, 12)
end)

minetest.register_craft({
	output = "nuke:mese_tnt 4",
	recipe = {
		{"", "group:wood", ""},
		{"default:mese_crystal", "default:coal_lump", "default:mese_crystal"},
		{"", "group:wood", ""},
	}
})


register_tnt(
	"nuke:mese_tntx", "Extreme Mese TNT",
	{"nuke_mese_tnt_top.png", "nuke_mese_tnt_bottom.png", "nuke_mese_tnt_side_x.png"},
	function(pos)
		on_explode_split(pos, 3, "nuke:mese_tnt")
	end
)

minetest.register_craft({
	output = "nuke:mese_tntx 1",
	recipe = {
		{"", "default:coal_lump", ""},
		{"default:coal_lump", "nuke:mese_tnt", "default:coal_lump"},
		{"", "default:coal_lump", ""},
	}
})

-- Compatibility aliases

minetest.register_alias("nuke:hardcore_iron_tnt", "nuke:iron_tntx")
minetest.register_alias("nuke:hardcore_mese_tnt", "nuke:mese_tntx")


if minetest.setting_getbool("log_mods") then
	print("[Nuke] Loaded")
end

local cannon_entity = {
	physical = true, -- collision
	collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
	visual = "wielditem",
	-- Slightly smaller than a normal node
	visual_size = {x=0.6, y=0.6, z=0.6},
	health = 12, -- number of punches required to defuse
	wield_item = "nuke:cannon_barrel",
	is_barrel = true
}

function cannon_entity:on_activate(staticdata)
	if staticdata and staticdata ~= "" then
		self.data = minetest.deserialize(staticdata)
	else
		self.data = {
			fire_timer = 0,
			ignite_time = 1,
			is_ignited = false
		}
	end
	self.object:set_armor_groups({punch_operable = 1})
end

function cannon_entity:ignite()
	self.data.fire_timer = self.data.ignite_time
	self.data.is_ignited = true
	
	local pos = self.object:get_pos()
	pos.y = pos.y + 0.5
	
	self.data.spark_spawner = minetest.add_particlespawner({
        amount = 64, -- 64
		time = 1,
		minpos = pos,
		maxpos = pos,
		minvel = {x = -0.25, y = 0.25, z = -0.25},
		maxvel = {x = 0.25, y = 1, z = 0.25},
		minacc = vector.new(),
		maxacc = vector.new(),
		minexptime = 1,
		maxexptime = 1,
		minsize = 0.5,
		maxsize = 0.5,
		texture = "spark.png"
    })
    
    minetest.sound_play("tnt_ignite", {pos = pos})
	
end

function cannon_entity:fire()

	local meta = minetest.get_meta(self.data.stand_pos)
	-- Get propellant
	local propellant_stack = meta:get_inventory():get_stack("propellant", 1)
	if propellant_stack and propellant_stack:get_count() > 0 then	
		
		-- Calculate firepower
		local used_gunpowder = math.min(10, propellant_stack:get_count())
		local vel_multiplier = math.log10(used_gunpowder + 1) * 64
		-- Take
		propellant_stack:take_item(used_gunpowder)
		meta:get_inventory():set_stack("propellant", 1, propellant_stack)
	
		-- Get ammunition
		local ammo_stack = meta:get_inventory():get_stack("ammunition", 1)
		if ammo_stack then
			-- Get pos in front of cannon
			local dir = minetest.yaw_to_dir(self.object:get_yaw())
			local pos = vector.add(self.object:get_pos(), vector.multiply(dir, 4))
			pos.y = pos.y + (4 * math.sin(self.object:get_rotation().x))
			
			local mult = {x=dir.x * 1.5, y=1.5 * math.sin(self.object:get_rotation().x), z=dir.z * 1.5}
			
			local ammo = ammo_stack:take_item(1)
			-- Update ammunition on inventory
			meta:get_inventory():set_stack("ammunition", 1, ammo_stack)
			
			minetest.add_particle({
				pos = pos,
				velocity = vector.new(),
				acceleration = vector.new(),
				expirationtime = 0.4,
				size = 30,
				collisiondetection = false,
				vertical = false,
				texture = "tnt_boom.png",
				glow = 15,
			})
			
			minetest.add_particlespawner({
				amount = 16,
				time = 0.5,
				minpos = vector.subtract(pos, 0.5),
				maxpos = vector.add(pos, 1),
				minvel = {x = -10, y = -10, z = -10},
				maxvel = {x = 10, y = 10, z = 10},
				minacc = vector.new(),
				maxacc = vector.new(),
				minexptime = 1,
				maxexptime = 2.5,
				minsize = 6,
				maxsize = 10,
				texture = "tnt_smoke.png",
			})
			
			minetest.sound_play("tnt_explode", {pos = pos, gain = 1.5, max_hear_distance = 2*64})
			
			local ammo_entity = minetest.add_entity(pos, ammo:get_name())
			if ammo_entity then
				local multiplier = vector.multiply(mult, vel_multiplier)
				ammo_entity:set_velocity(calculate_velocity({x=1,y=1,z=1}, 5, multiplier))
				ammo_entity:set_acceleration({x=0, y=-10, z=0})
				ammo_entity:set_yaw(self.object:get_yaw())
				-- Shells only explode on contact with something
				if ammo:get_name() ~= "nuke:shell_tnt" then
					ammo_entity:get_luaentity().timer = 4
				end
			end
		end	
	end

	self.data.is_ignited = false
	minetest.delete_particlespawner(self.data.spark_spawner)
end

function cannon_entity:on_step(dtime)

	self.data.fire_timer = self.data.fire_timer - dtime
	if self.data.fire_timer <= 0 and self.data.is_ignited then
		self:fire()
	end

	if self.data.driver then
		local driver_objref = minetest.get_player_by_name(self.data.driver)
		if driver_objref then
			local ctrl = driver_objref:get_player_control()
			local rotation = self.object:get_rotation()
			
			if ctrl.up then
				if rotation.x < math.pi / 8 then
					self.object:set_rotation({x=rotation.x + dtime * 0.9, y=rotation.y, z=rotation.z})
				end
				--driver_objref:set_look_vertical(driver_objref:get_look_vertical() + dtime * 1.25)
			
			elseif ctrl.down then
				if rotation.x > (-1 * math.pi / 8) then	
					self.object:set_rotation({x=rotation.x - dtime * 0.9, y=rotation.y, z=rotation.z})
				end
				--driver_objref:set_look_vertical(driver_objref:get_look_vertical() - dtime * 1.25)
			
			elseif ctrl.left then

				self.object:set_rotation({x=rotation.x, y=rotation.y + dtime * 0.45, z=rotation.z})
				--driver_objref:set_look_horizontal(driver_objref:get_look_horizontal() + dtime * 1.25)
				
				local objects = minetest.get_objects_inside_radius(self.object:get_pos(), 2)
				if objects then
					for _,obj in ipairs(objects) do
						if obj and obj:get_luaentity() and obj:get_luaentity().is_head then
							obj:set_yaw((self.object:get_yaw() - (math.pi/2)) + dtime * 0.45)
						end
					end
				end
				
			elseif ctrl.right then
	
				self.object:set_rotation({x=rotation.x, y=rotation.y - dtime * 0.45, z=rotation.z})
				--driver_objref:set_look_horizontal(driver_objref:get_look_horizontal() - dtime * 1.25)
				
				local objects = minetest.get_objects_inside_radius(self.object:get_pos(), 2)
				if objects then
					for _,obj in ipairs(objects) do
						if obj and obj:get_luaentity() and obj:get_luaentity().is_head then
							obj:set_yaw((self.object:get_yaw() - (math.pi/2)) - dtime * 0.45)
						end
					end
				end
			end
		end
	end
end

function cannon_entity:on_punch(clicker)
	
	-- Get wielded item
	local item = clicker:get_wielded_item()
	if item then
		local name = item:get_name()
		-- If item is a TNT, load it into ammo inventory
		if name == "nuke:tnt" or name == "nuke:iron_tnt" or name == "nuke:mese_tnt" or name == "nuke:shell_tnt" then
			local meta = minetest.get_meta(self.data.stand_pos)
			local placed = item:take_item(1)
			meta:get_inventory():add_item("ammunition", placed)
			clicker:set_wielded_item(item)
		elseif name == "tnt:gunpowder" then
			local meta = minetest.get_meta(self.data.stand_pos)
			local placed = item:take_item(1)
			meta:get_inventory():add_item("propellant", placed)
			clicker:set_wielded_item(item)
		elseif name == "default:torch" or name == "fire" then
			self:ignite()
		end
	end
	return false
end

function cannon_entity:on_rightclick(clicker)

	local name = clicker:get_player_name()
	if self.data.driver and name == self.data.driver then
		self.data.driver = nil
		clicker:set_detach()
		player_api.player_attached[name] = false
		player_api.set_animation(clicker, "stand" , 30)
		local pos = clicker:get_pos()
		pos = {x = pos.x, y = pos.y + 0.2, z = pos.z}
		minetest.after(0.1, function()
			clicker:set_pos(pos)
			clicker:set_eye_offset({x=0,y=0,z=0},{x=0,y=0,z=0})
		end)
	elseif not self.data.driver then
		local attach = clicker:get_attach()
		if attach and attach:get_luaentity() then
			local luaentity = attach:get_luaentity()
			if luaentity.data.driver then
				luaentity.data.driver = nil
			end
			clicker:set_detach()
		end
		self.data.driver = name
		local yaw = self.object:get_yaw()
		if yaw < 0 then yaw = yaw + math.pi else yaw = yaw - math.pi end
		local dir = minetest.yaw_to_dir(yaw)

		clicker:set_attach(self.object, "",
			{x=0,y=-10,z=-12}, {x = 0, y = 0, z = 0})
			
		clicker:set_eye_offset({x=0,y=2,z=-12},{x=0,y=0,z=0})

		player_api.player_attached[name] = true
		clicker:set_look_horizontal(self.object:get_yaw())
	end
end

function cannon_entity:get_staticdata()
	if self.data then
		return minetest.serialize(self.data)
	end
	return ""
end

minetest.register_entity("nuke:cannon_barrel", cannon_entity)

local mount_entity = {
	physical = true, -- collision
	collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
	visual = "wielditem",
	visual_size = {x=0.66, y=0.66, z=0.66},
	health = 1, -- number of punches required to defuse
	wield_item = "nuke:cannon_mount",
	is_head = true
}

minetest.register_entity("nuke:cannon_mount", mount_entity)

local function destroy_cannon_base(pos)
	-- TODO: Do nothing if cannon pieces are not in
	-- Clear cannon objects
	local objs = minetest.get_objects_inside_radius({x=pos.x, y=pos.y + 1.5, z=pos.z}, 1)
	for _,obj in pairs(objs) do
		obj:remove()
	end
	if objs and #objs > 0 then
		-- Pop-up items
		minetest.add_item(pos, "nuke:cannon_barrel")
		minetest.add_item(pos, "nuke:cannon_mount")
		local meta = minetest.get_meta(pos)
		local list = meta:get_inventory():get_list("ammunition")
		for _,item in pairs(list) do
			local drop_pos = {
				x=math.random(pos.x - 0.5, pos.x + 0.5),
				y=pos.y,
				z=math.random(pos.z - 0.5, pos.z + 0.5)}
			minetest.add_item(pos, item:to_string())
		end
		
		list = meta:get_inventory():get_list("propellant")
		for _,item in pairs(list) do
			local drop_pos = {
				x=math.random(pos.x - 0.5, pos.x + 0.5),
				y=pos.y,
				z=math.random(pos.z - 0.5, pos.z + 0.5)}
			minetest.add_item(pos, item:to_string())
		end
	end
	-- Remove node
	minetest.remove_node(pos)
end

function register_nodes()

	local cannon_node_def = {
		tiles = {
			"default_steel_block.png",
			"default_steel_block.png",
			"default_steel_block.png"..tube_entry_image,
			"default_steel_block.png"..tube_entry_image,
			"default_steel_block.png",
			"default_steel_block.png"
		},
		inventory_image = "cannon_base.png",
		drawtype = "nodebox",
		paramtype = "light",
		paramtype2 = "facedir",
		node_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5}, -- Normal cube
				{-1, -0.5, -1, 1, -0.25, 1}, -- Base
			}
		},
		groups = {cracky = 1, level = 2, tubedevice = 1, tubedevice_receiver = 1},
		description = "TNT Cannon Base",
		on_punch = function(pos, node, clicker)
			-- Get item, if item is a TNT, load it into ammo inventory
			local item = clicker:get_wielded_item()
			if item then
				local name = item:get_name()
				if name == "tnt:gunpowder" then
					local meta = minetest.get_meta(pos)
					local placed = item:take_item(1)
					meta:get_inventory():add_item("propellant", placed)
					clicker:set_wielded_item(item)
					return false
				end
			end
		end,
		on_dig = function(pos, node, digger)
			destroy_cannon_base(pos)
		end,
		on_blast = function(pos)
			destroy_cannon_base(pos)
			return nil
		end,
		tube = {
			insert_object = function(pos, node, stack, direction)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				--minetest.log("Direction: "..dump(direction))
				if direction.z == -1 then
					return inv:add_item("ammunition", stack)
				else
					return inv:add_item("propellant", stack)
				end
			end,
			can_insert = function(pos,node,stack,direction)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				--minetest.log("Direction: "..dump(direction))
				if direction.z == -1 then
					return inv:room_for_item("ammunition", stack)
				else
					return inv:room_for_item("propellant", stack)
				end
			end,
			input_inventory = "propellant",
			connect_sides = {left = 1, right = 1}
		},
		mesecons = {
			effector = {
				action_on = function(pos, node)
					local objs = minetest.get_objects_inside_radius({x=pos.x, y=pos.y + 2, z=pos.z}, 0.5)
					if objs then
						for _,obj in pairs(objs) do
							if obj and obj:get_luaentity().is_barrel then
								-- Ignite
								obj:get_luaentity():ignite()
							end
						end
					end
				end,
			},
		},
		allow_metadata_inventory_put = function(pos, listname, index, stack, player)
			if minetest.get_item_group(stack:get_name(), "nuke_tnt") ~= 0 or stack:get_name() == "tnt:gunpowder" then
				return stack:get_count()
			end
			return 0
		end,
	}

	if minetest.get_modpath("pipeworks") then
		cannon_node_def.after_place_node = pipeworks.after_place
		cannon_node_def.after_dig_node = pipeworks.after_dig
	end

	minetest.register_node("nuke:cannon_base", cannon_node_def)
	
	-- Cannon pieces
	minetest.register_node("nuke:cannon_barrel", {
		description = "TNT Cannon Barrel",
		tiles = {
			"default_steel_block.png",
			"default_steel_block.png",
			"default_steel_block.png",
			"default_steel_block.png",
			"cannon_front.png",
			"default_steel_block.png"
		},
		inventory_image = "cannon_barrel.png",
		drawtype = "nodebox",
		paramtype = "light",
		paramtype2 = "facedir",
		node_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -1.5, 0.5, 0.5, 3.5},
			}
		},
		groups = {cracky = 1, level = 2},
		on_construct = function(pos)
			local cannon_base_pos = {x=pos.x, y=pos.y-2, z=pos.z}
			local cannon_mount_pos = {x=pos.x, y=pos.y-1, z=pos.z}
			local cannon_base_node = minetest.get_node(cannon_base_pos)
			local cannon_mount_node = minetest.get_node(cannon_mount_pos)

			if cannon_base_node and cannon_mount_node then
				if cannon_base_node.name == "nuke:cannon_base" 
					and cannon_mount_node.name == "nuke:cannon_mount" then
					-- Remove both nodes
					minetest.remove_node(pos)
					minetest.remove_node(cannon_mount_pos)
					
					-- Add the entities
					local cannon = minetest.add_entity(pos, "nuke:cannon_barrel")
					local mount = minetest.add_entity(cannon_mount_pos, "nuke:cannon_mount")
					cannon:get_luaentity().data.stand_pos = cannon_base_pos
					cannon:set_yaw(cannon:get_yaw() + (math.pi/2))
					
					-- Setup inventories
					local meta = minetest.get_meta(cannon_base_pos)
					local inv = meta:get_inventory()
					inv:set_size("propellant", 1*1)
					inv:set_size("ammunition", 1*1)
					meta:set_string("formspec", "size[8,8]"..
						"label[2,0.25;Propellant]"..
						"label[5,0.25;Ammunition]"..
						"list[nodemeta:"..cannon_base_pos.x..","..cannon_base_pos.y..","..cannon_base_pos.z..";ammunition;5,1;1,1]"..
						"list[nodemeta:"..cannon_base_pos.x..","..cannon_base_pos.y..","..cannon_base_pos.z..";propellant;2,1;1,1]"..
						"list[current_player;main;0,3;8,4]"..
						"listring[]")
				end
			end
		end,
	})

	minetest.register_node("nuke:cannon_mount", {
		description = "TNT Cannon Barrel Mount",
		tiles = {
			"default_steel_block.png",
			"default_steel_block.png",
			"default_steel_block.png",
			"default_steel_block.png",
			"default_steel_block.png",
			"default_steel_block.png"
		},
		inventory_image = "cannon_mount.png",
		drawtype = "nodebox",
		paramtype = "light",
		paramtype2 = "facedir",
		node_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0, -1, 0.5, 1.25, -0.5}, -- NodeBox2
				{-0.5, -0, 1, 0.5, 1.25, 0.5}, -- NodeBox5
				{-0.5, -0.5, -0.5, 0.5, 0.25, 0.5}, -- NodeBox6
			}
		},
		groups = {cracky = 1, level = 2}
	})
end

minetest.register_craft({
	output = "nuke:cannon_mount",
	recipe = {
		{"default:steel_block", "", "default:steel_block"},
		{"default:steel_block", "default:steel_block", "default:steel_block"},
		{"", "default:steel_block", ""},
	}
})

minetest.register_craft({
	output = "nuke:cannon_base",
	recipe = {
		{"", "default:chest", ""},
		{"", "default:steel_block", ""},
		{"default:steel_block", "default:steel_block", "default:steel_block"},
	}
})

minetest.register_craft({
	output = "nuke:cannon_barrel",
	recipe = {
		{"tnt:gunpowder", "", ""},
		{"default:steel_block", "default:steel_block", "default:steel_block"},
		{"", "", ""},
	}
})

register_nodes()
